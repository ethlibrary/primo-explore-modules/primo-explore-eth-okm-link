# primo-explore-eth-okm-link

## Description

This Module integrates links to Open Knowledge Maps (Databases: Base and PubMed) in Primo VE.
See https://openknowledgemaps.org/.
It adds a facet group "Send my search to" at the end of the facets. The facet group contains two links to Open Knowlege Maps, one that queries the BASE database and one that queries PubMed.
The search term is taken from the search slot in Primo VE.

### Screenshot

#### Open Knowledge Maps links
![screenshot](https://gitlab.com/ethlibrary/primo-explore-modules/primo-explore-eth-okm-link/-/raw/master/screenshot1.jpg)

## Installation

1. Assuming you've installed and are using [primo-explore-devenv](https://github.com/ExLibrisGroup/primo-explore-devenv).

2. Navigate to your view root directory. For example:
    ```
    cd primo-explore/custom/MY_VIEW_ID
    ```
3. If you do not already have a package.json file in this directory, create one:
    ```
    npm init -y
    ```
4. Install this package:
    ```
    npm install primo-explore-eth-okm-link --save-dev
    ```

5. Copy the image Logo_Open_Knowledge_Maps.jpg from the img directory primo-explore/custom/MY_VIEW_ID/node_modules/primo-explore-eth-okm-link/img to the img directory of your view primo-explore/custom/MY_VIEW_ID/img.


## Usage

Once installed, inject `ethOkmLinkModule` as a dependency, and then add the eth-okm-link-component directive to the prmFacetExactAfter component.

```js

import 'primo-explore-eth-okm-link';

var app = angular.module('viewCustom', ['ethOkmLinkModule']);

app.component('prmFacetExactAfter',  {
        bindings: {parentCtrl: '<'},
        template: `<eth-okm-link-component after-ctrl="$ctrl"></eth-okm-link-component>`
    })
```
